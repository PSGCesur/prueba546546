package models;

import java.sql.ResultSet;

public interface RegionDAO {
	//crud
	public ResultSet selectAllRegions();
	public Region insertRegion(int id, String nombre);
	public boolean deleteRegion(int id);
	public Region updateRegion(int id, String nombre);
	//extra
	public ResultSet selectRegionByName(String name);
}
