package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import controllers.RegionController;
import models.Region;

public class Pruebas {

	public static void main(String[] args) {
		//characterEncoding=UTF-8
		//jdbc:mysql://127.0.0.1:3306/?user=root
		String cadena_conexion="jdbc:mysql://localhost:3306/hr?useUnicode=true&serverTimezone=UTC";
		String usuario = "root";
		String contrasena = "1234";
		
		try {
			Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();
			//RegionController rc = new RegionController(conn);
			try {
				PreparedStatement stm;
				ResultSet rs = null;
			
				stm = conn.prepareStatement("SELECT * FROM REGIONS");
				rs = stm.executeQuery();

				while(rs.next()) {
					int id  = rs.getInt("region_id");
					String nombre = rs.getString("region_name");
					Region r1 = new Region(id, nombre);
					System.out.println(r1);
				}
				
				rs.close();		
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				
			}

			System.out.println("Fin");
		}
	}

}
