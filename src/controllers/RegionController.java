package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.Region;
import models.RegionDAO;

public class RegionController implements RegionDAO{

	private final String SELECT_ALL = "select * from regions";
	
	private Connection con;
	
	public RegionController(Connection con) {
		this.con = con;
	}

	@Override
	public ResultSet selectAllRegions() {
		PreparedStatement stm;
		ResultSet rs = null;
		try {
			stm = this.con.prepareStatement(SELECT_ALL);
			rs = stm.executeQuery();
			
			//stm.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}

	@Override
	public Region insertRegion(int id, String nombre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteRegion(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Region updateRegion(int id, String nombre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet selectRegionByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
